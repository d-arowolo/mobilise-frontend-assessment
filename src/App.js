import React, { Component } from "react";
import "./App.css";
import { BrowserRouter as Router, Route } from "react-router-dom";
import Layout from "./components/Layout";
import Login from "./components/Login";
import Product from "./components/Product";
import Cart from "./components/Cart";

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      products: [
        {
          _id: "1",
          title: "Creative Headline Hand Bags",
          src: [
            "https://images.unsplash.com/photo-1566150905458-1bf1fc113f0d?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1051&q=80",
            "https://images.unsplash.com/photo-1566150902887-9679ecc155ba?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1yZWxhdGVkfDF8fHxlbnwwfHx8fA%3D%3D&auto=format&fit=crop&w=500&q=60",
            "https://images.unsplash.com/photo-1566150905968-62f0de3d6df2?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1yZWxhdGVkfDN8fHxlbnwwfHx8fA%3D%3D&auto=format&fit=crop&w=500&q=60",
            "https://images.unsplash.com/photo-1566150905890-ee196a98d9b5?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1050&q=80",
          ],
          description: "Nice hand bags",
          content:
            "Lorem ipsum dolor sit, amet consectetur adipisicing elit.Reprehenderit est aliquam incidunt consequatur quod, excepturi expedita esse nulla, nihil deleniti aliquid optio itaque odit repellendus repudiandae perspiciatis asperiores laudantiumarchitecto tempora sequi cupiditate corporis beatae! Namaspernatur ad error praesentium odit eum enim perspiciatis dolor temporibus.",
          price: 20,
          colors: ["peach", "brown", "yellow", "pink"],
          count: 1,
        },
      ],
      index: 0,
      cartItems: [
        {
          _id: "1",
          title: "Creative Headline Hand Bags",
          src: [
            "https://images.unsplash.com/photo-1566150905458-1bf1fc113f0d?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1051&q=80",
            "https://images.unsplash.com/photo-1566150902887-9679ecc155ba?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1yZWxhdGVkfDF8fHxlbnwwfHx8fA%3D%3D&auto=format&fit=crop&w=500&q=60",
            "https://images.unsplash.com/photo-1566150905968-62f0de3d6df2?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1yZWxhdGVkfDN8fHxlbnwwfHx8fA%3D%3D&auto=format&fit=crop&w=500&q=60",
            "https://images.unsplash.com/photo-1566150905890-ee196a98d9b5?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1050&q=80",
          ],
          description: "Nice hand bags",
          content:
            "Lorem ipsum dolor sit, amet consectetur adipisicing elit.Reprehenderit est aliquam incidunt consequatur quod, excepturi expedita esse nulla, nihil deleniti aliquid optio itaque odit repellendus repudiandae perspiciatis asperiores laudantiumarchitecto tempora sequi cupiditate corporis beatae! Namaspernatur ad error praesentium odit eum enim perspiciatis dolor temporibus.",
          price: 20,
          colors: ["peach", "brown", "yellow", "pink"],
          count: 1,
          qty: 1,
        },
      ],
    };
  }

  myRef = React.createRef();

  handleTab = (index) => {
    this.setState({ index: index });
    const images = this.myRef.current.children;
    for (let i = 0; i < images.length; i++) {
      images[i].className = images[i].className.replace("active", "");
    }
    images[index].className = "active";
  };

  componentDidMount() {
    this.setState({
      cartItems: this.state.cartItems,
    });
  }

  onAdd = (product) => {
    const exist = this.state.cartItems.find((x) => x._id === product._id);
    if (exist) {
      this.setState({
        cartItems: this.state.cartItems.map((x) =>
          x._id === product._id && exist.qty < 10
            ? { ...exist, qty: exist.qty + 1 }
            : x
        ),
      });
    } else {
      this.setState({
        cartItems: [...this.state.cartItems, { ...product, qty: 1 }],
      });
      //
    }
    console.log(this.state.cartItems);
  };

  onRemove = (product) => {
    const exist = this.state.cartItems.find((x) => x._id === product._id);
    if (exist.qty > 1) {
      this.setState({
        cartItems: this.state.cartItems.map((x) =>
          x._id === product._id ? { ...exist, qty: exist.qty - 1 } : x
        ),
      });
    }
  };

  onClose = (product) => {
    this.setState({
      cartItems: this.state.cartItems.filter((x) => x.id !== product.id),
    });
  };

  render() {
    const { products, index, cartItems } = this.state;
    return (
      <Layout>
        <Router>
          <Route exact path="/" render={(props) => <Login />} />
          <Route
            path="/product"
            render={(props) => (
              <Product
                products={products}
                index={index}
                handleTab={this.handleTab}
                myRef={this.myRef}
                onAdd={this.onAdd}
              />
            )}
          />
          <Route
            path="/cart"
            render={(props) => (
              <Cart
                cartItems={cartItems}
                onAdd={this.onAdd}
                onRemove={this.onRemove}
                onClose={this.onClose}
                index={index}
              />
            )}
          />
        </Router>
      </Layout>
    );
  }
}

export default App;
