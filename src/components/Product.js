import React, { useEffect } from "react";

const Product = (props) => {
  const { products, index, handleTab, myRef, onAdd } = props;

  useEffect(() => {
    const { index } = props;
    myRef.current.children[index].className = "active";
  });

  return (
    <div className="app">
      {products.map((item) => (
        <div className="details" key={item._id}>
          <div className="big-img">
            <img src={item.src[index]} alt="" />
            <div className="thumb" ref={myRef}>
              {item.src.map((img, index) => (
                <img
                  src={img}
                  alt=""
                  key={index}
                  onClick={() => handleTab(index)}
                />
              ))}
            </div>
          </div>

          <div className="box">
            <div className="row">
              <h2>{item.title}</h2>
              <p className="price">£{item.price}</p>
            </div>
            <div className="colors">
              {item.colors.map((color, index) => (
                <button
                  style={{ background: color }}
                  key={index}
                  onClick={() => handleTab(index)}
                ></button>
              ))}
            </div>

            <p>{item.description}</p>
            <p>{item.content}</p>

            <button className="cart" onClick={() => onAdd(item)}>
              Add to cart
            </button>
          </div>
        </div>
      ))}
    </div>
  );
};

export default Product;
