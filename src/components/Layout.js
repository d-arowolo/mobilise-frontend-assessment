import React, { Component } from "react";
import { Container } from "reactstrap";
import NavMenu from "./NavMenu";

class Layout extends Component {
  render() {
    return (
      <div>
        <NavMenu />
        <div className="myBody">
          <Container>{this.props.children}</Container>
        </div>
      </div>
    );
  }
}

export default Layout;
