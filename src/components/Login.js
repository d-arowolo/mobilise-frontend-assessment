import React from "react";
import { Button, FormGroup, Label, Input } from "reactstrap";
import "../containers/login.css";

const Login = () => {
  return (
    <div className="login">
      <div className="card">
        <h5>Sign in</h5>
        <FormGroup>
          <Input
            type="text"
            name="username"
            id="username"
            placeholder="Username or email address"
          />
        </FormGroup>
        <FormGroup>
          <Input
            type="password"
            name="password"
            id="password"
            placeholder="Password"
          />
        </FormGroup>
        <FormGroup check>
          <Label>
            <Input type="checkbox" /> Keep me signed in
          </Label>
        </FormGroup>
        <Button
          color="primary"
          className="login-button"
          // onClick={props.login}
        >
          Login
        </Button>
        <p>
          <a href="/">I forgot my password</a>
        </p>
        <p>
          <a href="/">Resend verification email</a>
        </p>
      </div>
    </div>
  );
};

export default Login;
