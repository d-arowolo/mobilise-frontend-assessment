import React from "react";
import { Button, FormGroup, Form, Label, Input } from "reactstrap";
import "../containers/cart.css";

const Cart = (props) => {
  const { cartItems, index, onAdd, onRemove, onClose } = props;
  const month = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
  const year = [];
  for (let i = 2021; i < 2030; i++) {
    year.push(<option>{i}</option>);
  }

  return (
    <div className="row">
      <div className="block col-8">
        <div>
          <h2>Shopping Cart</h2>
          <div className="card-items">
            {cartItems.length === 0 && <div>Cart is empty</div>}
            {cartItems.map((item) => (
              <div key={item.id} className="row">
                <div className="round-thumb col-3">
                  <img src={item.src[index]} alt="" />
                </div>
                <div className="col-4">{item.title}</div>
                <div className="col-2">
                  <div className="row">
                    <Button
                      onClick={() => onRemove(item)}
                      className="remove col-3 primary"
                    >
                      -
                    </Button>
                    <Input value={item.qty} disabled className="col-4 mx-1" />
                    <Button
                      onClick={() => onAdd(item)}
                      className="add col-3 primary"
                    >
                      +
                    </Button>
                  </div>
                </div>

                <div className="col-1">£{item.qty * item.price.toFixed(2)}</div>
                <div
                  className="col-1 close btn-primary-outline"
                  onClick={onClose}
                >
                  X
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>
      <div className="block col-4 card-details">
        <h2>Card Details</h2>
        <Label className="mt-4">Card Type</Label>
        <div className="atm-card"></div>
        <Form>
          <FormGroup>
            <Label>Name on Card</Label>
            <Input className="input" />
          </FormGroup>
          <FormGroup>
            <Label>Card Number</Label>
            <Input placeholder="**** **** **** ****" className="input" />
          </FormGroup>
          <FormGroup>
            <div className="row">
              <div className="col-9">
                <Label>Expiration date</Label>
                <div className="row">
                  <Input
                    type="select"
                    name="month"
                    className="col-4 mx-3 input"
                    placeholder="mm"
                  >
                    {month.map((x, i) => {
                      return <option key={i}>{x}</option>;
                    })}
                  </Input>
                  <Input
                    type="select"
                    name="year"
                    className="col-5 mx-3 input"
                    placeholder="yyyy"
                  >
                    {year}
                  </Input>
                </div>
              </div>
              <div className="col-3">
                <Label>CVV</Label>
                <Input placeholder="XXX" className="input" />
              </div>
            </div>
          </FormGroup>
          <FormGroup></FormGroup>
          <Button color="primary" size="md" block>
            Check Out
          </Button>
        </Form>
      </div>
    </div>
  );
};

export default Cart;
